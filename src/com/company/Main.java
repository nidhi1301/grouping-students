package com.company;

import java.util.ArrayList;
import static com.company.GroupStudent.*;
import static com.company.PrintList.printList;
import static com.company.RankStudent.rank;


public class Main {


    public static void main(String[] args) {
        ArrayList<Student> studentList = new ArrayList<>();

        studentList.add(new Student("nidhi", 70.0, 76.5, 86.0, 89.0));
        studentList.add(new Student("vatsal", 70.0, 89, 87.0, 39.0));
        studentList.add(new Student("anjul", 89.0, 16.5, 66.0, 79.0));
        studentList.add(new Student("diya", 90.0, 45.5, 68.0, 89.0));
        studentList.add(new Student("pratibha", 40.8, 36.5, 56.0, 99.0));
        studentList.add(new Student("bhawna", 40.0, 76.5, 86.0, 99.0));
        studentList.add(new Student("samyukta", 30.0, 76.5, 86.0, 89.0));
        studentList.add(new Student("bikash", 70.0, 76.5, 86.0, 89.0));

        group(studentList);
        printList(computerStudentList);
        System.out.println("-------------");
        printList(biologyStudentList);
        System.out.println("-------------");
        printList(commerceStudentList);
        System.out.println("-------------");

        rank(studentList);

    }
}
