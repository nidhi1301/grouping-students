package com.company;

import java.util.ArrayList;

public class GroupStudent {
    static ArrayList<Student> computerStudentList = new ArrayList<>();
    static ArrayList<Student> biologyStudentList = new ArrayList<>();
    static ArrayList<Student> commerceStudentList = new ArrayList<>();

    public static void group(ArrayList<Student> studentList) {
        for (Student student : studentList) {
            double pcmPercentage = ((student.mathsMarks + student.chemistryMarks + student.physicsMarks) / 300.0) * 100.0;

            if (pcmPercentage > 70.0 && student.CSMarks > 80.0) {
                computerStudentList.add(student);
            } else if (pcmPercentage > 70.0) {
                biologyStudentList.add(student);
            } else if (student.mathsMarks > 80.0) {
                commerceStudentList.add(student);
            }

        }
    }
}
