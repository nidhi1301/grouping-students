package com.company;

public class Student {
    private String name;
    double mathsMarks;
    double chemistryMarks;
    double physicsMarks;
    double CSMarks;
    double totalPercentage = 0;

    public Student(String name, double mathsMarks, double chemistryMarks, double physicsMarks, double CSMarks) {
        this.name = name;
        this.mathsMarks = mathsMarks;
        this.chemistryMarks = chemistryMarks;
        this.physicsMarks = physicsMarks;
        this.CSMarks = CSMarks;

        this.totalPercentage = ((mathsMarks + chemistryMarks + physicsMarks + CSMarks) / 400.0) * 100.0;
    }

    public String toString() {//overriding the toString() method
        return name + " " + mathsMarks + " " + chemistryMarks + " " + physicsMarks + " " + CSMarks;
    }


}
