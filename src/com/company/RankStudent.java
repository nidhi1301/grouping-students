package com.company;

import java.util.ArrayList;

import static com.company.PrintList.printList;

public class RankStudent {
    public static void rank(ArrayList<Student> studentList) {
        ArrayList<Student> copy_studentList = new ArrayList<>();
        copy_studentList.addAll(studentList);

        copy_studentList = studentList;
        copy_studentList.sort((o1, o2)
                -> Double.valueOf(o1.totalPercentage).compareTo(o2.totalPercentage));

        printList(copy_studentList);
    }

}